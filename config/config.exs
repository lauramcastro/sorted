# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :sorted,
  ecto_repos: [Sorted.Repo]

# Configures the endpoint
config :sorted, SortedWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vIGMpD8qqkuDElWFlOAYF1t20csItbwdgyAB7tWhngY5o/rqYRqxTZzozR8AIaMe",
  render_errors: [view: SortedWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Sorted.PubSub,
  live_view: [signing_salt: "CRdtlXeUqvmSdepjMOhBEdAkvI6lvcXg"]

# Configures gettext
config :sorted, SortedWeb.Gettext,
  default_locale: "gl",
  locales: ~w(en es gl)

# Configures Elixir's Logger
config :logger,
  backends: [:console, {LoggerFileBackend, :output_log}]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :logger, :output_log,
  format: "[$level] $message\n",
  path: "priv/output.csv",
  metadata_filter: [output: true]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
