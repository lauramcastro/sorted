# SortED

Aplicación web en Elixir/Phoenix para realizar o proceso de asignación automática de propostas de TFGs.


## Inicio da aplicación

Dado que o proxecto parte do esqueleto dunha aplicación Elixir/Phoenix, despois de descargar ou clonar este repositorio, o servidor pode arrincarse do seguinte xeito:

  * Instalando as dependencias con `mix deps.get`
  * Inicializando os diferentes compoñentes con `mix setup`
  * Arrincando o servidor Phoenix con `mix phx.server`

Nese momento debe poder visitarse o enderezo [`localhost:4000`](http://localhost:4000) desde calquera navegador, e visualizar a pantalla de inicio:

<img src="./doc/welcome_screen.png" alt="SortED homepage (desktop)" width="800"/>

Para a instalación de Erlang/Elixir e PostgreSQL (prerrequisitos), pode consultarse o [sitio web de Phoenix](https://hexdocs.pm/phoenix/installation.html). Para o detalle do formato agardado nos documentos a aportar coa información sobre as propostas, solicitantes e solicitudes, consúltese a [documentación](https://gitlab.com/lauramcastro/sorted/-/tree/main/doc/specs).

Unha vez subidos os arquivos requiridos, activarase o botón `Imos aló!` co que se poderá iniciar o proceso de asignación automática, que se detalla a continuación.

<img src="./doc/ready_screen.png" alt="SortED ready to proceed (desktop)" width="800"/>

## Asignación automática de propostas de TFGs

A arquitectura da aplicación está representada no seguinte diagrama:

<img src="./doc/diagrams/SortED.png" alt="SortED architecture" width="600"/>

Cando o sistema se inicia, o supervisor global (`Sorted.Application`) inicia o supervisor xeral da lóxica (`Sorted.TopSupervisor`). Este supervisor é dinámico, de xeito que cando se invoca o método `Sorted.TopSupervisor.start_children/0`, inicia a estrutura de procesos que representa, por unha banda, as propostas realizadas e, por outra, as solicitudes rexistradas (cos seus respectivos supervisores). Tamén hai dous procesos dedicados, respectivamente, a custodiar a información académica e estatística.

<img src="./doc/diagrams/SortED (startup sequence).png" alt="SortED startup sequence" width="600"/>

Desde a interface web, a invocación do ḿétodo `Sorted.TopSupervisor.start_children/0` prodúcese cando se preme o botón `Imos aló!`.

Cando se inician, os procesos `Request` comezan a comunicarse cos `Proposal` de xeito que fagan constar as súas preferencias de asignación e a cualificación que lles corresponde, criterio fundamental polo que serán admitidos ou rexeitados.

O seguinte diagrama representa a primeira petición de cada proceso `Request`, que involucra averiguar a cualificación antes de comezar a facer peticións. O proceso `Academic` é o depositario desta información. Para cada petición a unha `Proposal`, o proceso `Request` precisa coñecer a identidade da proposta, información da que é depositario o proceso `Stats`. Nótese que o sistema, ao estar baseado en procesos, é fundamentalmente asíncrono: cada invocación a método (marcada `deste_xeito()`) vén seguida do envío dunha mensaxe (marcada `msg:deste xeito`). Entre o envío da mensaxe e o seu procesado (marcado `handle msg:deste xeito`) pode pasar un tempo indeterminado, que no diagrama se representa cunha discontinuidade.

<img src="./doc/diagrams/SortED (simple request sequence).png" alt="SortED simple request sequence" width="600"/>

Como vemos, despois de recibir a aceptación da súa solicitude, os procesos `Request` non rematan, senón que permanecen en agarda.
Por suposto, un proceso `Request` tamén pode recibir unha resposta negativa a unha solicitude:

<img src="./doc/diagrams/SortED (denied request sequence).png" alt="SortED denied request sequence" width="600"/>

Tras unha resposta negativa, pode darse que o proceso `Request` non teña máis opcións preferidas que solicitar, e nese caso deterase. O seu supervisor non o arrincará de novo:

<img src="./doc/diagrams/SortED (denied and give up request sequence).png" alt="SortED denied and give up request sequence" width="300"/>

Nalgúns casos, podería ser que o proceso `Request` non teña preferencias, pero queira unha asignación de todos os xeitos; nese caso, permanecerá en agarda:

<img src="./doc/diagrams/SortED (denied and wait request sequence).png" alt="SortED denied and wait request sequence" width="300"/>

O escenario máis complexo resulta cando un proceso `Request` solicita unha `Proposal` xa asignada, pero para a que ten maior nota que algún dos procesos `Request` que foran aceptados por esa `Proposal`. Nesa situación substitúese a `Request` de menor nota (que pasa a rexeitarse) pola recén chegada (que se acepta):

<img src="./doc/diagrams/SortED (request and replace sequence).png" alt="SortED request and replace sequence" width="600"/>

Cando rematan as negociacións, pódese dar por finalizado o procedemento a través dos supervisores:

`iex> Elixir.Sorted.RequestsSupervisor.settle_requests`

`iex> Elixir.Sorted.ProposalsSupervisor.settle_proposals`

Desde a interface web, a invocación destes métodos prodúcese cando se preme o botón `Rematar`.
