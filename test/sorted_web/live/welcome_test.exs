defmodule SortedWeb.PageLiveTest do
  use SortedWeb.ConnCase

  import Phoenix.LiveViewTest

  # test "connected render", %{conn: conn} do
  #   {:ok, page_live, _} = live(conn, "/")
  #   assert render(page_live) =~ "Welcome to Sorted!"
  # end

  # test "disconnected render", %{conn: conn} do
  #   {:ok, _, disconnected_html} = live(conn, "/")
  #   assert disconnected_html =~ "Welcome to Sorted!"
  # end

  test "presence", %{conn: conn} do
    {:ok, page_live, _} = live(conn, "/")
    assert render(page_live) =~ "There is currently 1 user on this page"
  end
end
