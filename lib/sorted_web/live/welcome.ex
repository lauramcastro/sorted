defmodule SortedWeb.Welcome do
  @moduledoc """
  This module defines the welcome page.
  """
  use Phoenix.LiveView
  import SortedWeb.Gettext, only: [gettext: 1, ngettext: 3]
  import Gettext, only: [with_locale: 2]

  alias Sorted.Documents
  require Logger
  require Sorted.Configuration

  @presence_topic "presence"

  @impl true
  def mount(_params, session, socket) do
    {:ok, _} = SortedWeb.Presence.track(self(), @presence_topic, socket.id, %{})
    :ok = SortedWeb.Endpoint.subscribe(@presence_topic)

    initial_present = SortedWeb.Presence.list(@presence_topic) |> map_size

    socket =
      assign(socket, present: initial_present)
      |> assign(propostas: Documents.list_upload(Sorted.Configuration.proposals()))
      |> assign(solicitantes: Documents.list_upload(Sorted.Configuration.students()))
      |> assign(solicitudes: Documents.list_upload(Sorted.Configuration.requests()))

    locale =
      case session["locale"] do
        nil -> Gettext.get_locale()
        locale -> locale
      end

    {:ok, assign(socket, locale: locale)}
  end

  @impl true
  def handle_event("start", _, socket) do
    Logger.debug("Starting the sorting process!")
    Sorted.TopSupervisor.start_children()
    {:noreply, socket}
  end

  def handle_event("stop", _, socket) do
    Logger.debug("Settling the sorting process...")
    Sorted.RequestsSupervisor.settle_requests()
    Sorted.ProposalsSupervisor.settle_proposals()
    {:noreply, socket}
  end

  @impl true
  def handle_info(
        %{event: "presence_diff", payload: %{joins: joins, leaves: leaves}},
        socket = %{assigns: %{present: present}}
      ) do
    new_present = present + map_size(joins) - map_size(leaves)

    {:noreply, assign(socket, :present, new_present)}
  end

  @impl true
  def render(assigns) do
    ~L"""
    <%= with_locale(@locale, fn -> %>
    <div align="center">
    <%= ngettext("There is currently 1 user on this page",
                 "There are currently %{count} users on this page", @present) %>

    <br/>
    <br/>

    <div class="w3-container" style="width:90%;max-width:1024px;">

      <div class="w3-row">

        <!-- images come from https://www.iconfinder.com/search/icons?family=kameleon-icons&price=free&license=gte__2 -->
        <!-- images come from https://nicubunu.blogspot.com/2007/01/ampelfrau-and-ampelmaennchen.html -->

        <div class="w3-third">
          <div class="w3-card-4 w3-light-grey" style="width:90%;max-width:420px;margin:10px;">
            <header class="w3-container w3-dark-grey">
              <h3><%= gettext("Project proposals") %></h3>
            </header>
            <div class="w3-container">
              <p><%= if @propostas do %><img src="/images/gruen_ampelfrau.png" alt="<%= gettext("Green light") %>" style="width:80px"/>
                 <% else %><img src="/images/rot_ampelfrau.png" alt="<%= gettext("Red light") %>" style="width:80px"/>
                 <% end %></p>
              <hr/>
              <img src="/images/proposal.png" alt="<%= gettext("Proposal avatar") %>" class="w3-left w3-margin-right" style="width:80px"/>
              <p><%= gettext("Available project proposals made by faculty members.") %></p>
            </div>
            <a href="/uploads/new?propostas" class="w3-button w3-block w3-padding-large w3-white">+ <%= gettext("Upload") %></a>
          </div>
        </div>

        <div class="w3-third">
          <div class="w3-card-4 w3-light-gray" style="width:90%;max-width:420px;margin:10px;">
            <header class="w3-container w3-dark-grey">
              <h3><%= gettext("Students") %></h3>
            </header>
            <div class="w3-container">
              <p><%= if @solicitantes do %><img src="/images/gruen_ampelfrau.png" alt="<%= gettext("Green light") %>" style="width:80px"/>
              <% else %><img src="/images/rot_ampelfrau.png" alt="<%= gettext("Red light") %>" style="width:80px"/>
              <% end %></p>
              <hr/>
              <img src="/images/students.png" alt="<%= gettext("Students avatar") %>" class="w3-left w3-margin-right" style="width:80px"/>
              <p><%= gettext("Students who have made requests for project assigment.") %></p>
            </div>
            <a href="/uploads/new?solicitantes" class="w3-button w3-block w3-padding-large w3-white">+ <%= gettext("Upload") %></a>
          </div>
        </div>

        <div class="w3-third">
          <div class="w3-card-4 w3-light-grey" style="width:90%;max-width:420px;margin:10px;">
            <header class="w3-container w3-dark-grey">
              <h3><%= gettext("Project requests") %></h3>
            </header>
            <div class="w3-container">
              <p><%= if @solicitudes do %><img src="/images/gruen_ampelfrau.png" alt="<%= gettext("Green light") %>" style="width:80px"/>
              <% else %><img src="/images/rot_ampelfrau.png" alt="<%= gettext("Red light") %>" style="width:80px"/>
              <% end %></p>
              <hr/>
              <img src="/images/checklist.png" alt="<%= gettext("Requests avatar") %>" class="w3-left w3-margin-right" style="width:80px;"/>
              <p><%= gettext("List of assigment requests submitted by the students.") %></p>
            </div>
            <a href="/uploads/new?solicitudes" class="w3-button w3-block w3-padding-large w3-white">+ <%= gettext("Upload") %></a>
          </div>
        </div>

      </div>

      <br/>
      <%= gettext("For information on document format, please") %>
      <a href="https://gitlab.com/lauramcastro/sorted/-/blob/main/README.md" target="_blank" style="text-decoration:underline;"><%= gettext("refer to the documentation") %></a>.
      <br/>
      <br/>

      <button phx-click="start" <%= if not (@propostas and @solicitantes and @solicitudes) do %>disabled<% end %> style="margin:10px;"><%= gettext("Go") %></button>
      <button phx-click="stop" <%= if not (@propostas and @solicitantes and @solicitudes) do %>disabled<% end %> style="margin:10px;"><%= gettext("Settle") %></button>

      <br/><br/>

    </div>
    </div>
    <br/>
    <% end) %>
    """
  end
end
