defmodule SortedWeb.UploadController do
  use SortedWeb, :controller
  alias Sorted.Documents
  alias Sorted.Documents.Upload

  require Logger

  def new(conn, parameters) do
    [focus_doc] = Map.keys(parameters)
    render(conn, "new.html", focus_doc: Gettext.gettext(SortedWeb.Gettext, focus_doc))
  end

  def create(conn, %{"upload" => %Plug.Upload{} = upload, "focus_doc" => focus_doc}) do
    Logger.debug("New upload")

    case Documents.create_upload_from_plug_upload(upload, focus_doc) do
      {:ok, _upload} ->
        put_flash(conn, :info, gettext("File uploaded correctly"))
        |> redirect(to: Routes.upload_path(conn, :index))

      {:error, reason} ->
        put_flash(
          conn,
          :error,
          gettext("Error uploading file: %{error_desc}", error_desc: "#{inspect(reason)}")
        )
        |> render("new.html")
    end
  end

  def create(conn, _upload) do
    Logger.debug("No file to upload")

    put_flash(conn, :error, gettext("No file selected"))
    |> redirect(to: Routes.upload_path(conn, :index))
  end

  def index(conn, params) do
    Logger.debug("Showing list of files #{inspect(params)}")
    uploads = Documents.list_uploads()
    render(conn, "index.html", uploads: uploads)
  end

  def show(conn, %{"id" => id}) do
    upload = Documents.get_upload!(id)
    local_path = Upload.local_path(upload.id, upload.filename)
    send_download(conn, {:file, local_path}, filename: upload.filename)
  end
end
