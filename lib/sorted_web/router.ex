defmodule SortedWeb.Router do
  use SortedWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SortedWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SortedWeb.Plugs.Locale
  end

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  scope "/", SortedWeb do
    pipe_through :browser

    live "/", Welcome
    # get "/", PageController, :index
    # get "/ola", GreetingsController, :index
    # get "/ola/:visitante", GreetingsController, :show
    resources "/uploads", UploadController, only: [:index, :new, :create, :show]
    # resources "/posts", PostController, only: [:index, :show]
    # resources "/comments", CommentController, except: [:delete]
  end

  # Other scopes may use custom stacks.
  # scope "/api", SortedWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SortedWeb.Telemetry
    end
  end
end
