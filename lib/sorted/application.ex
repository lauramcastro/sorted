defmodule Sorted.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Sorted.Repo,
      # Start the Telemetry supervisor
      SortedWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Sorted.PubSub},
      # Start Presence
      SortedWeb.Presence,
      # Start the Endpoint (http/https)
      SortedWeb.Endpoint,
      # Start a worker by calling: Sorted.Worker.start_link(arg)
      # {Sorted.Worker, arg}
      {Sorted.TopSupervisor, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Sorted.AppSupervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SortedWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
