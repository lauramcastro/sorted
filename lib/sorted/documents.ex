defmodule Sorted.Documents do
  @moduledoc """
  This module defines the logic of creating files.
  """
  import Ecto.Query, warn: false

  alias Sorted.Repo
  alias Sorted.Documents.Upload

  def list_uploads do
    Repo.all(Upload)
  end

  def list_upload(name) do
    Repo.exists?(from u in Upload, where: ilike(u.filename, ^"#{name}.csv"))
  end

  def get_upload!(id) do
    Upload
    |> Repo.get!(id)
  end

  def get_upload_by_name(name) do
    from(u in Upload, where: ilike(u.filename, ^"#{name}.csv")) |> last() |> Repo.one()
  end

  def create_upload_from_plug_upload(
        %Plug.Upload{
          filename: filename,
          path: tmp_path,
          content_type: content_type
        },
        focus_doc
      ) do
    # upload creation logic

    hash =
      File.stream!(tmp_path, [], 2048)
      |> Upload.sha256()

    Repo.transaction(fn ->
      with {:ok, %File.Stat{size: size}} <- File.stat(tmp_path),
           {:ok, upload} <-
             %Upload{}
             |> Upload.changeset(%{
               filename: focus_doc <> ".csv",
               content_type: content_type,
               hash: hash,
               size: size
             })
             |> Repo.insert(),
           :ok <-
             File.cp(
               tmp_path,
               Upload.local_path(upload.id, focus_doc <> ".csv")
             ) do
        {:ok, upload}
      else
        {:error, reason} -> Repo.rollback(reason)
      end
    end)
  end
end
