defmodule Sorted.Academic do
  @moduledoc """
  This module defines the logic of the academic secretariat.

  Implemented as a `GenServer`, it holds information on academic preformance and replies to requests with their own info only.
  """
  use GenServer

  alias Sorted.Documents
  alias Sorted.Documents.Upload

  require Logger
  require Sorted.Configuration

  # API

  @doc """
  Starts the academic secretariat process.
  """
  @spec start_link() :: GenServer.on_start()
  def start_link() do
    Logger.debug("Starting gen_server process: academic info")
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Relies to the academic secretariat process a request for academic information.

  It requires both the PID of the request (`who`) and its registered name (`what`).
  """
  @spec request(pid(), atom()) :: :ok
  def request(who, what) do
    GenServer.cast(__MODULE__, {:academic_info, who, what})
  end

  # GenServer callbacks

  @impl true
  def init([]) do
    data = Documents.get_upload_by_name(Sorted.Configuration.students())

    processed_data =
      Upload.local_path(data.id, data.filename)
      |> Path.expand()
      |> Path.absname()
      |> File.read!()
      |> String.split(Sorted.Configuration.eol_separator(), trim: true)
      |> Enum.map(fn student ->
        String.split(student, Sorted.Configuration.field_separator()) |> process
      end)
      |> Map.new()

    {:ok, processed_data}
  end

  @impl true
  def handle_cast({:academic_info, pid, id}, state) do
    {_name, _surname, _itinerary, _tfg, value} = state[id]
    Sorted.Request.provide_value(pid, value)
    {:noreply, state}
  end

  defp process([id, name, surname, itinerary, tfg, value]) do
    {String.to_atom(id),
     {name, surname, itinerary, Sorted.LogicCommon.is_true?(tfg), String.to_float(value)}}
  end
end
