defmodule Sorted.Configuration do
  @moduledoc """
  This module defines macros that are used in different components of the system logic.
  """
  defmacro eol_separator, do: ~s(\n)
  defmacro field_separator, do: ~s(\t)
  defmacro itineraries_name_cs, do: "Computación"
  defmacro itineraries_name_ce, do: "Enxeñaría de Computadores"
  defmacro itineraries_name_se, do: "Enxeñaría do Software"
  defmacro itineraries_name_is, do: "Sistemas de Información"
  defmacro itineraries_name_it, do: "Tecnoloxías da Información"
  defmacro itineraries_separator, do: ~s(;)
  defmacro proposal_name, do: "prop_"
  defmacro proposals, do: "propostas"
  defmacro requests, do: "solicitudes"
  defmacro students, do: "solicitantes"
end
