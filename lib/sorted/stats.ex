defmodule Sorted.Stats do
  @moduledoc """
  This module defines the logic of the stats compiler.

  Implemented as a `GenServer`, it can identify proposal names with proposal ids and compilates stats on proposal interest (i.e. number of requests).
  """
  use GenServer

  require Logger
  require Sorted.Configuration

  # API

  @doc """
  Starts the stats process.
  """
  @spec start_link([Supervisor.child_spec()]) :: GenServer.on_start()
  def start_link(data) do
    Logger.debug("Starting gen_server process: stats")
    GenServer.start_link(__MODULE__, data, name: __MODULE__)
  end

  @doc """
  Relies a request for a given proposal id (given a proposal name) to the stats process.
  """
  @spec request(GenServer.server(), String.t()) :: :ok
  def request(who, proposal_name) do
    GenServer.cast(__MODULE__, {:request, who, proposal_name})
  end

  @doc """
  Shuts down the stats process.
  """
  @spec settle(pid()) :: :ok
  def settle(who) do
    GenServer.stop(who)
  end

  # GenServer callbacks

  @impl true
  def init(data) do
    {:ok, process_data(data, [])}
  end

  @impl true
  def handle_cast({:request, pid, proposal_name}, state) do
    {value, _proposal_name} = List.keyfind(state, proposal_name, 1)
    Sorted.Request.provide_value(pid, value)
    {:noreply, state}
  end

  # Internal functions

  @spec process_data([Supervisor.child_spec()], [{atom(), String.t()}]) :: [{atom(), String.t()}]
  defp process_data([], acc), do: acc

  defp process_data([%{id: _id, start: {_m, _f, [data, _opts]}} | moredata], acc) do
    stream = String.splitter(data, Sorted.Configuration.field_separator())
    [id, title] = stream |> Enum.take(2)
    name = String.to_atom(Sorted.Configuration.proposal_name() <> id)
    process_data(moredata, [{name, String.trim(title)} | acc])
  end
end
