defmodule Sorted.Repo do
  use Ecto.Repo,
    otp_app: :sorted,
    adapter: Ecto.Adapters.Postgres
end
