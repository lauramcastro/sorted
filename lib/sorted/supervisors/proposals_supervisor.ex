defmodule Sorted.ProposalsSupervisor do
  @moduledoc """
  This module defines the supervisor responsible for proposals.

  It will start and keep alive as many children as proposals are described in a given CSV file (one per line).
  """
  use Supervisor

  require Logger
  require Sorted.Configuration

  @doc """
  Starts the proposals supervisor, and in turn, all proposals it supervises.

  Additionally, it also starts a statistical process that serves as directory for proposals.
  """
  @spec start_link() :: Supervisor.on_start()
  def start_link() do
    Logger.debug("Starting supervisor of: proposals")
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Settles all proposals that the supervisor is currently managing.

  It returns a boolean value that represents whether all proposals were successfully settled.
  """
  @spec settle_proposals() :: boolean()
  def settle_proposals() do
    Sorted.SupervisorCommon.settle(Supervisor.which_children(__MODULE__))
    Supervisor.count_children(__MODULE__)[:active] == 0
  end

  @impl true
  def init([]) do
    # List all child processes to be supervised: one per Proposal
    children =
      Sorted.SupervisorCommon.children_specs(
        Sorted.Proposal,
        Sorted.Configuration.proposals(),
        Sorted.Configuration.proposal_name()
      )

    Logger.debug(
      "Supervisor of proposals will handle: " <>
        to_string(length(children)) <> " proposals and one stats process"
    )

    stats = %{id: :stats, start: {Sorted.Stats, :start_link, [children]}, restart: :transient}

    opts = [strategy: :one_for_one]
    Supervisor.init([stats | children], opts)
  end
end
