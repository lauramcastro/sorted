defmodule Sorted.RequestsSupervisor do
  @moduledoc """
  This module defines the supervisor responsible for requests.

  It will start and keep alive as many children as requests are described in a given CSV file (one per line).
  """
  use Supervisor

  require Logger
  require Sorted.Configuration

  @doc """
  Starts the requests supervisor, and in turn, all requests it supervises.
  """
  @spec start_link() :: Supervisor.on_start()
  def start_link() do
    Logger.debug("Starting supervisor of: requests")
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Settles all requests that the supervisor is currently managing.

  It returns a boolean value that represents whether all requests were successfully settled.
  """
  @spec settle_requests() :: boolean()
  def settle_requests() do
    Sorted.SupervisorCommon.settle(Supervisor.which_children(__MODULE__))
    Supervisor.count_children(__MODULE__)[:active] == 0
  end

  @impl true
  def init([]) do
    # List all child processes to be supervised: one per Request
    children =
      Sorted.SupervisorCommon.children_specs(
        Sorted.Request,
        Sorted.Configuration.requests()
      )

    Logger.debug(
      "Supervisor of requests will handle: " <> to_string(length(children)) <> " requests"
    )

    opts = [strategy: :one_for_one]
    Supervisor.init(children, opts)
  end
end
