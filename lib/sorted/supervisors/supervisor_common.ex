defmodule Sorted.SupervisorCommon do
  @moduledoc """
  This module defines common functionalities for business logic supervisors.
  """
  alias Sorted.Documents
  alias Sorted.Documents.Upload

  require Sorted.Configuration

  @doc """
  Given a data source (namely, a file) and a child type, the source is processed to obtain a list of child specs (as required by `Supervisor`).

  The source is processed in a lazy manner (using `Stream`). Optionally, a label can be provided that will be used as prefix to name the children.
  """
  @spec children_specs(atom(), String.t(), String.t()) :: list(Supervisor.child_spec())
  def children_specs(child_type, data_source, label \\ "") do
    data = Documents.get_upload_by_name(data_source)

    Upload.local_path(data.id, data.filename)
    |> Path.expand()
    |> Path.absname()
    |> File.stream!([], :line)
    |> Stream.map(&String.trim(&1))
    |> Stream.map(&child_spec(child_type, label, &1))
    |> Enum.to_list()
  end

  @spec child_spec(atom(), String.t(), Enumerable.t()) :: Supervisor.child_spec()
  defp child_spec(child_type, label, data) do
    stream = String.splitter(data, Sorted.Configuration.field_separator())
    [id] = stream |> Enum.take(1)
    name = String.to_atom(label <> id)

    %{
      id: id,
      start: {child_type, :start_link, [data, [name: name]]},
      restart: :transient
    }
  end

  @doc """
  Recursively invokes a `settle/1` function for a list of children, as obtained by `Supervisor.which_children/1`.
  """
  @spec settle([Supervisor.child_spec()]) :: :ok
  def settle([]), do: :ok
  def settle([{_id, :undefined, :worker, [_module]} | children]), do: settle(children)

  def settle([{_id, pid, :worker, [module]} | children]) do
    apply(module, :settle, [pid])
    settle(children)
  end
end
