defmodule Sorted.TopSupervisor do
  @moduledoc """
  This module defines the supervisor responsible for the logic-related supervisors of the system.

  It will start and keep alive the proposals supervisor (`Sorted.ProposalsSupervisor`) and the
  requests supervisor (`Sorted.RequestsSupervisor`), as well as the academic secretariat process
  (`Sorted.Academic`).

  It is a dynamic supervisor, so it can be started at application start-up, but does not start
  its children until the data is available. Specifically, the two children-supervisors could
  be started, but the child-process not.
  """
  use DynamicSupervisor

  require Logger

  @doc """
  Starts the top-level supervisor of the business logic.
  """
  @spec start_link(list(any())) :: Supervisor.on_start()
  def start_link(init_arg) do
    Logger.debug("Starting supervisor of: top level")
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @spec start_children() :: boolean
  def start_children() do
    proposals_sup_spec = %{
      id: Sorted.ProposalsSupervisor,
      start: {Sorted.ProposalsSupervisor, :start_link, []},
      restart: :transient
    }

    requests_sup_spec = %{
      id: Sorted.RequestsSupervisor,
      start: {Sorted.RequestsSupervisor, :start_link, []},
      restart: :transient
    }

    academic_spec = %{
      id: Sorted.Academic,
      start: {Sorted.Academic, :start_link, []},
      restart: :transient
    }

    proposals_up = DynamicSupervisor.start_child(__MODULE__, proposals_sup_spec)
    requests_up = DynamicSupervisor.start_child(__MODULE__, requests_sup_spec)
    academic_up = DynamicSupervisor.start_child(__MODULE__, academic_spec)

    start_ok(proposals_up) and start_ok(requests_up) and start_ok(academic_up)
  end

  defp start_ok({:ok, pid}) when is_pid(pid), do: true
  defp start_ok({:error, {:already_started, pid}}) when is_pid(pid), do: true
  defp start_ok(_), do: false
end
