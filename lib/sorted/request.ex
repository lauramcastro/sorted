defmodule Sorted.Request do
  @moduledoc """
  This module defines the logic of a request.

  Implemented as a `GenServer`, requests will try to contact their corresponding proposals, and wait for an asynchronous notification of whether they are the best match or not.
  """
  use GenServer

  require Logger
  require Sorted.Configuration

  # API

  @doc """
  Starts a request process.
  """
  @spec start_link(list(any()), list(any())) :: GenServer.on_start()
  def start_link(data, options \\ []) do
    GenServer.start_link(__MODULE__, data, options)
  end

  @doc """
  Relies a requested value (either academic or proposal-related) to its request(er).
  """
  @spec provide_value(GenServer.server(), float() | atom()) :: :ok
  def provide_value(who, value) when is_float(value) do
    GenServer.cast(who, {:academic_info, value})
  end

  def provide_value(who, value) when is_atom(value) do
    GenServer.cast(who, {:proposal_info, value})
  end

  @doc """
  Relies the acceptance of a request
  """
  @spec accept(GenServer.server(), pid()) :: :ok
  def accept(who, from) do
    GenServer.cast(who, {:accepted, from})
  end

  @doc """
  Relies the denial of a request
  """
  @spec deny(GenServer.server(), pid()) :: :ok
  def deny(who, from) do
    GenServer.cast(who, {:denied, from})
  end

  @doc """
  Settles a given request, meaning that if the request has been assigned, it will gracefully shut down.
  """
  @spec settle(GenServer.server()) :: :ok
  def settle(who) do
    GenServer.cast(who, :settle)
  end

  # GenServer callbacks

  @impl true
  def init(request) do
    [id, name, pref1, pref2, pref3, pref4, pref5, others, itinerary] =
      String.split(request, Sorted.Configuration.field_separator())

    Logger.debug("Starting request by " <> name <> " (" <> id <> ")")
    schedule_perform_request()

    {:ok,
     {String.to_atom(id), name, for(p when p != "" <- [pref1, pref2, pref3, pref4, pref5], do: p),
      Sorted.LogicCommon.is_true?(others), itinerary, :unknown, :unassigned}}
  end

  @impl true
  def handle_cast(
        :settle,
        state = {id, name, _prefs, true, itinerary, value, :unassigned}
      ) do
    Logger.debug(
      "Request " <> to_string(id) <> " asked to settle, ignores because it is unassigned"
    )

    Logger.info(
      "\t" <> name <> "\t" <> itinerary <> "\t" <> to_string(value) <> "\tgranted NO request",
      output: true
    )

    {:noreply, state}
  end

  def handle_cast(
        :settle,
        state = {id, name, _prefs, _others, itinerary, value, {assignment_id, assignment}}
      ) do
    Logger.debug(
      "Request " <>
        to_string(id) <>
        " asked to settle, complies with final assigment " <> to_string(assignment_id)
    )

    Logger.info(
      "\t" <>
        name <>
        "\t" <> itinerary <> "\t" <> to_string(value) <> "\tgranted request\t" <> assignment,
      output: true
    )

    {:stop, :normal, state}
  end

  def handle_cast(
        :settle,
        state = {id, name, _prefs, false, itinerary, value, :unassigned}
      ) do
    Logger.debug(
      "Request " <> to_string(id) <> " asked to settle, complies despite not being assigned"
    )

    Logger.info(
      "\t" <> name <> "\t" <> itinerary <> "\t" <> to_string(value) <> "\tgranted NO request",
      output: true
    )

    {:noreply, state}
  end

  def handle_cast(
        {:academic_info, value},
        {id, name, prefs, others, itinerary, :unknown, assignment}
      ) do
    Logger.debug("Request " <> to_string(id) <> " receives grade [" <> to_string(value) <> "]")
    schedule_perform_request()
    {:noreply, {id, name, prefs, others, itinerary, value, assignment}}
  end

  def handle_cast(
        {:proposal_info, pref_id},
        {id, name, [pref | more_prefs], others, itinerary, value, assignment}
      )
      when not is_atom(pref) do
    Logger.debug("Request " <> to_string(id) <> " receives info about " <> to_string(pref_id))
    schedule_perform_request()
    {:noreply, {id, name, [{pref_id, pref} | more_prefs], others, itinerary, value, assignment}}
  end

  def handle_cast(
        {:accepted, proposal},
        {id, name, [{pref_id, pref} | more_prefs], others, itinerary, value, _assigment}
      ) do
    Logger.debug(
      "Request " <> to_string(id) <> " gets accepted [assigned " <> to_string(pref_id) <> "]"
    )

    if Process.whereis(pref_id) == proposal do
      {:noreply, {id, name, more_prefs, others, itinerary, value, {pref_id, pref}}}
    end
  end

  def handle_cast(
        {:denied, proposal},
        {id, name, [{pref_id, _pref} | more_prefs], others, itinerary, value, :unassigned}
      ) do
    Logger.debug(
      "Request " <> to_string(id) <> " is denied [not assigned " <> to_string(pref_id) <> "]"
    )

    if Process.whereis(pref_id) == proposal do
      schedule_perform_request()
      {:noreply, {id, name, more_prefs, others, itinerary, value, :unassigned}}
    end
  end

  def handle_cast(
        {:denied, proposal},
        {id, name, prefs, others, itinerary, value, {assigment_id, _assigment}}
      ) do
    Logger.debug(
      "Request " <>
        to_string(id) <> " is denied [no longer assigned " <> to_string(assigment_id) <> "]"
    )

    if Process.whereis(assigment_id) == proposal do
      schedule_perform_request()
      {:noreply, {id, name, prefs, others, itinerary, value, :unassigned}}
    end
  end

  @impl true
  def handle_info(
        :request_next_pref,
        state = {id, name, [], false, itinerary, value, :unassigned}
      ) do
    Logger.debug(
      "Request " <>
        to_string(id) <> " has no further prefs nor wants a different assignment [stopping]"
    )

    Logger.info(
      "\t" <>
        name <>
        "\t" <>
        itinerary <>
        "\t" <> to_string(value) <> "\thas no further requests nor wants any other assigment",
      output: true
    )

    {:stop, :normal, state}
  end

  def handle_info(
        :request_next_pref,
        state = {id, _name, [], true, _itinerary, _value, :unassigned}
      ) do
    Logger.debug(
      "Request " <>
        to_string(id) <> " has no further prefs but wants an assignment anyway [waiting]"
    )

    {:noreply, state}
  end

  def handle_info(
        :request_next_pref,
        state = {id, _name, _prefs, _others, _itinerary, :unknown, _assignment}
      ) do
    Logger.debug("Request " <> to_string(id) <> " inquiring Academic about grade")
    Sorted.Academic.request(self(), id)
    {:noreply, state}
  end

  def handle_info(
        :request_next_pref,
        state =
          {id, _name, [{next_pref_id, _next_pref} | _more_prefs], _others, itinerary, value,
           _assignment}
      ) do
    Logger.debug(
      "Request " <>
        to_string(id) <>
        " requests " <>
        to_string(next_pref_id) <>
        " [itinerary=" <> itinerary <> " grade=" <> to_string(value) <> "]"
    )

    Sorted.Proposal.request(next_pref_id, self(), itinerary, value)
    {:noreply, state}
  end

  def handle_info(
        :request_next_pref,
        state = {id, _name, [next_pref | _more_prefs], _others, _itinerary, _value, _assignment}
      ) do
    Logger.debug("Request " <> to_string(id) <> " inquiring Stats about " <> next_pref)
    Sorted.Stats.request(self(), next_pref)
    {:noreply, state}
  end

  @impl true
  def terminate(
        :normal,
        {_id, _name, _prefs, _others, _itinerary, _value, _assignment}
      ) do
    :ok
  end

  # Internal functions

  @spec schedule_perform_request() :: reference()
  defp schedule_perform_request() do
    Process.send_after(self(), :request_next_pref, trunc(:rand.uniform() * 1_000))
  end
end
