defmodule Sorted.Proposal do
  @moduledoc """
  This module defines the logic of a proposal.

  Implemented as a `GenServer`, proposals will wait for requests and remember the one(s) which is/are (a) best match(es), notifying said requests accordingly.
  """
  use GenServer

  require Logger
  require Sorted.Configuration

  @type proposal() :: {
          id :: String.t(),
          title :: String.t(),
          itineraries :: [String.t()],
          instances :: integer(),
          assignees :: [pid()]
        }

  @callback init(String.t()) :: {:ok, state :: proposal()}
  @callback handle_cast(action, state) :: :ok
            when action: :settle | {:request, pid(), String.t(), float()},
                 state: proposal()

  # API

  @doc """
  Starts a proposal process.
  """
  @spec start_link(String.t(), list(any())) :: GenServer.on_start()
  def start_link(data, options \\ []) do
    GenServer.start_link(__MODULE__, data, options)
  end

  @doc """
  Relies a request to a specific proposal. The request includes the PID of the corresponding process, as well as the itinerary (which must be a match), and a numeric ranking value (for priority ordering).
  """
  @spec request(GenServer.server(), pid(), String.t(), float()) :: :ok
  def request(proposal, who, itinerary, value) do
    GenServer.cast(proposal, {:request, who, itinerary, value})
  end

  @doc """
  Settles a given proposal, meaning that if the proposal has been assigned (fully), it will gracefully shut down.
  """
  @spec settle(GenServer.server()) :: :ok
  def settle(which) do
    GenServer.cast(which, :settle)
  end

  # GenServer callbacks

  @impl true
  def init(proposal) do
    [id, title, _supervisors, itineraries, instances, _type] =
      String.split(proposal, Sorted.Configuration.field_separator())

    Logger.debug("Starting proposal '" <> title <> "' (" <> to_string(id) <> ")")

    {:ok,
     {id, title,
      String.split(itineraries, Sorted.Configuration.itineraries_separator(), trim: true),
      String.to_integer(instances), []}}
  end

  @impl true
  def handle_cast(:settle, state = {id, _title, _itineraries, instances, _assignees})
      when instances == 0 do
    Logger.debug(
      "Proposal " <>
        to_string(id) <> " asked to settle, complies because it has been fully assigned"
    )

    {:stop, :normal, state}
  end

  def handle_cast(:settle, state = {id, _title, _itineraries, instances, _assignees})
      when instances > 0 do
    Logger.debug(
      "Proposal " <> to_string(id) <> " asked to settle, ignores because it is not fully assigned"
    )

    {:noreply, state}
  end

  def handle_cast(
        {:request, from, itinerary, value},
        state = {id, title, itineraries, instances, assignees}
      )
      when instances > 0 do
    if Enum.member?(itineraries, itinerary) do
      Logger.debug(
        "Proposal " <>
          to_string(id) <>
          " with room for " <>
          to_string(instances) <>
          " assignment receives new request and accepts [value=" <>
          to_string(value) <> "]"
      )

      Sorted.Request.accept(from, self())

      {:noreply,
       {id, title, itineraries, instances - 1, List.keysort([{from, value} | assignees], 1)}}
    else
      Logger.debug(
        "Proposal " <>
          to_string(id) <>
          " with room for " <>
          to_string(instances) <>
          " assignment(s) receives new request but refuses [wrong itinerary: " <>
          itinerary <> "]"
      )

      Sorted.Request.deny(from, self())
      {:noreply, state}
    end
  end

  def handle_cast(
        {:request, from, itinerary, value},
        state = {id, title, itineraries, 0, assignees}
      ) do
    {action, new_state} =
      if Enum.member?(itineraries, itinerary) do
        competitors =
          for(
            {who, who_value} when who_value < value <- assignees,
            do: {who, who_value}
          )

        if competitors == [] do
          Logger.debug(
            "Proposal " <>
              to_string(id) <>
              " with no room for further assignment receives new request but refuses [low grade] "
          )

          {:deny, state}
        else
          Logger.debug(
            "Proposal " <>
              to_string(id) <>
              " with no room for further assignment receives new request and complies "
          )

          {replaced, replaced_value} = hd(List.keysort(competitors, 1))
          Sorted.Request.deny(replaced, self())

          {:accept,
           {id, title, itineraries, 0,
            [{from, value} | assignees] -- [{replaced, replaced_value}]}}
        end
      else
        Logger.debug(
          "Proposal " <>
            to_string(id) <>
            " with no room for further assignment receives new request but refuses [wrong itinerary: " <>
            itinerary <> "]"
        )

        {:deny, state}
      end

    apply(Sorted.Request, action, [from, self()])
    {:noreply, new_state}
  end

  @impl true
  def terminate(:normal, {_id, _title, _itineraries, _instances, _assignees}) do
    :ok
  end
end
