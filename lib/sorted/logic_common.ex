defmodule Sorted.LogicCommon do
  @moduledoc """
  This module defines utility functions used in different components of the business logic.

  """

  @doc """
  Checks whether or not a given string value represents a boolean one.
  """
  @spec is_true?(String.t()) :: boolean
  def is_true?(value), do: String.downcase(value) |> truth_value

  @spec truth_value(String.t()) :: boolean
  defp truth_value("si"), do: true
  defp truth_value("sí"), do: true
  defp truth_value("no"), do: false
  defp truth_value("non"), do: false
end
